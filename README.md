# tdd-kata-scaffold

This is the scaffolding for the TDD kata exercises, found for instance here: https://kata-log.rocks/

AVA: https://github.com/avajs/ava
fast-check: https://dubzzz.github.io/fast-check
