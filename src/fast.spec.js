const test = require('ava')
const fc = require('fast-check')
const { contains, fib } = require('./fast')

test('should always contain itself', t => {
  try { fc.assert(fc.property(fc.string(), text => contains(text, text))) } catch(e) { t.fail(e.message) }
  t.pass()
})

test('should always contain its substrings', t => {
  try { fc.assert(fc.property(fc.string(), fc.string(), fc.string(), (a, b, c) => contains(a + b + c, b))) } catch(e) { t.fail(e.message) }
  t.pass()
})

test('should always have length of n', t => {
  try { fc.assert(fc.property(fc.integer(3, 10000), (n) => fib(n).length === n)) } catch(e) { t.fail(e.message) }
  t.pass()
})
