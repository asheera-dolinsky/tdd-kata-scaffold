function fizzBuzz (n) {
  if (n % 3 === 0 && n % 5 === 0) return 'FizzBuzz'
  if (n % 3 === 0) return 'Fizz'
  if (n % 5 === 0) return 'Buzz'
}

module.exports = {
  fizzBuzz,
  fizzBuzzGenerator: function*(s, e) {
    while(s <= e) {
      const r = fizzBuzz(s)
      if(r) yield [s, r]
      s++
    }
  }
}
