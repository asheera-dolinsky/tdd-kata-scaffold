const test = require('ava')
const { fizzBuzz, fizzBuzzGenerator } = require('./fizz-buzz')

test('3 returns Fizz', t => {
  t.is(fizzBuzz(3), 'Fizz')
})

test('5 returns Buzz', t => {
  t.is(fizzBuzz(5), 'Buzz')
})

test('15 returns FizzBuzz', t => {
  t.is(fizzBuzz(15), 'FizzBuzz')
})

test('FizzBuzz integration should return correct output for the [1, 15]', t => {
  const result = []
  for(let [i, s] of fizzBuzzGenerator(1, 15)) result.push([i, s])
  t.deepEqual(result, [
    [3, 'Fizz'],
    [5, 'Buzz'],
    [6, 'Fizz'],
    [9, 'Fizz'],
    [10, 'Buzz'],
    [12, 'Fizz'],
    [15, 'FizzBuzz']
  ])
})
