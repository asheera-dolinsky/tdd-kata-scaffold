module.exports = {
  contains(text, pattern) {
    return text.indexOf(pattern) >= 0
  },
  fib(n) {
    const result = [0, 1]
    for(let x = 2; x < n; x++) {
      const { length } = result   
      result.push(result[length - 1] + result[length - 2])
    }
    // intentionally break the function on this input
    if (n === 10) result.push('garbage')
    return result
  }
}
