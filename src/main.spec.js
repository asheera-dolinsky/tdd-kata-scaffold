const test = require('ava')
const { hello } = require('./main')

test('always pass', t => {
	t.pass()
})

test('hello', t => {
  t.is(hello(), 'hello')
})
